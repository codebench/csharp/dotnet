﻿using System;

namespace async
{
    class Program
    {
        private static async void RunBigTask() {
            Console.WriteLine("RunBigTask()");
	}
	
        static async void Main(string[] args)
        {
//            Console.WriteLine("Hello World!");


            await Program.RunBigTask();


        }
    }
}
